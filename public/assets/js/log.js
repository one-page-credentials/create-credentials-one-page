var getCanvas;

$(document).ready(function() { 
	$('.pop-up').hide();
	var element = $("#card-credentials");

	$("#download").on('click', function() { 
		const link = document.createElement('a');
        link.download = 'CredentialsCard.png';
	  	link.href = getCanvas.toDataURL("image/png");
	  	link.click();
	}); 

	$("#Generated").click(function(){
		var username;
		var password;
		var link;
		var projek_name;

		if (document.getElementById("username").value == '') {
			username 	= "-";
			// $("#DataUsername").hide();
		} else {
			username 	= document.getElementById("username").value;
		}

		if (document.getElementById("password").value == '') {
			password 	= "-";
			// $("#DataPassword").hide();
		} else {
			password 	= document.getElementById("password").value;
		}

		if (document.getElementById("link").value == '') {
			link 	= " ";
			// $("#DataLink").hide();
		} else {
			// $("#DataLink").show();
			link 	= document.getElementById("link").value;
		}

		if (document.getElementById("namaproject").value == '') {
			projek_name 	= " ";
			// $("#DataProjek").hide();
		} else {
			// $("#DataProjek").show();
			projek_name 	= document.getElementById("namaproject").value;
		}

		var txtUsername 	= document.getElementById("txtUsername");
		var txtPassword 	= document.getElementById("txtPassword");
		var txtLink			= document.getElementById("txtLink");
		var txtProjek_name  = document.getElementById("txtProjek_name");

		txtUsername.innerHTML 		= username;
		txtPassword.innerHTML 		= password;
		txtLink.innerHTML 			= link;
		txtProjek_name.innerHTML 	= projek_name;

		$(".pop-up").fadeIn('slow');

		html2canvas(document.querySelector("#card-credentials"), {
			allowTaint: true
		}).then(canvas => {
            // $("#buka").append(canvas);
            getCanvas = canvas;
        });
	});

	$("#close").click(function(){
		$(".pop-up").fadeOut('slow');
	});
});