<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Someah Credentials</title>

    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&family=Roboto&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Miriam+Libre&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/ini.css">
    <link rel="stylesheet" href="assets/css/output.css">
    <link rel="stylesheet" href="assets/css/preview.css">

    <script src="assets/js/html2canvas.js"></script>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/log.js"></script>
</head>
<body>
	<header>
                <img id="logo" src="assets/images/someah-logo-white.png" alt="">
            </header>
             <section class="container my-1">
                 <div class="row">
                     <div class="col-lg-5">
                 <div id="text" class="">
                    <h5 id="judul">Input <span id="cred">Credentials</span></h5>
                 </div><br>
                 <form method="POST">
                     <div id="rowatas" class="input-name shadow p-2 bg-white my-3">
                         <div class="row">
                            <div class="col-sm-2">
                                <img src="assets/images/avatar.png" class="size-img mt-2 ml-2" alt="">
                            </div>
                            <div>
                                <input type="text" class="form-control input-bg" name="Username" id="username" placeholder=" Email or Username">
                            </div>
                         </div>
                     </div>
                     <div class="input-name shadow p-2 bg-white my-3">
                        <div class="row">
                           <div class="col-sm-2">
                               <img src="assets/images/password.png" class="size-img mt-2 ml-2" alt="">
                           </div>
                           <div>
                               <input type="text" class="form-control input-bg" name="Password" id="password" placeholder=" Password">
                           </div>
                        </div>
                    </div>
                    <div class="input-name shadow p-2 bg-white my-3">
                        <div class="row">
                           <div class="col-sm-2">
                               <img src="assets/images/link.png" class="size-img mt-2 ml-2" alt="">
                           </div>
                           <div>
                               <input type="text" class="form-control input-bg" name="Link" id="link" placeholder=" Link">
                           </div>
                        </div>
                    </div>
                    <div class="input-name shadow p-2 bg-white my-3">
                        <div class="row">
                           <div class="col-sm-2">
                               <img src="assets/images/project-management.png" class="size-img mt-2 ml-2" alt="">
                           </div>
                           <div>
                               <input type="text" class="form-control input-bg" name="NamaProject" id="namaproject" placeholder=" Nama Project">
                           </div>
                        </div>
                    </div>
                 </form>
                <div class=" my-3">
                    <button type="submit" id="Generated" type="button" class="btn2 p-2">Generate</button>
                </div>
                </div>
                <div class="col-lg-5">
                    <img src="assets/images/HeadingImage.png" id="side-image" alt="Credentials Someah">
                </div>
                </div>
                </section> 
            
            <!-- Optional JavaScript -->
            <!-- jQuery first, then Popper.js, then Bootstrap JS -->
            <div class="pop-up position-fixed">
              <div data-role="main" class="ui-content">
                <div id="close" class="btn btn-white shadow mt-2">
                  <b>X</b>
                </div>
                <h1 data-rel="popup" class="ui-btn-inline ui-corner-all mt-5" data-position-to="window" >
                  <center> 
                      <font size="6">Pastikan data sudah benar! Jika sudah silahkan klik kanan dan save image.</font>
                      </br>
                  </center>
                </h1>
                <div id="card-credentials" class="output-card">
                  <div>
                    <header class="container">
                        <div>
                            <img id="logo" src="assets/images/someah-logo-white.png" alt="logo">
                            <h5 id="id-card">PT. Someah Kreatif Nusantara</h5>
                            <p class="alamat">Jl. Cisitu  Indah III No. 2, Dago, Coblong, Kota Bandung, Jawa Barat, 40135 </p>
                            <hr class="featurette-divider">  
                        </div>
                    </header>
                    <br>
                    <br>
                    <section class="container isi-out">
                      <div>   
                          <div>
                              <div class="row row2">
                                  <div class="col-sm-3">
                                      <p>Username</p>
                                  </div>
                                  <div>
                                      <p>: <span id="txtUsername"></span></p>
                                  </div>
                              </div>
                          </div>
                          <div>
                              <div class="row row2">
                                  <div class="col-sm-3">
                                      <p>Password</p>
                                  </div>
                                  <div>
                                      <p>: <span id="txtPassword"></span></p>
                                  </div>
                              </div>
                          </div>
                          <div>
                              <div id="DataProjek" class="row row2">
                                  <div>
                                      <p><span id="txtProjek_name"></span></p>
                                  </div>
                              </div>
                          </div>
                          <div>
                              <div id="DataLink" class="row row2">
                                  <div>
                                      <p><span id="txtLink" class="font-blue"></span></p>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </section>
                    <section class="ini-scan">
                      <div class="container">
                          <div>
                              <img class="scan" src="assets/images/frame.png" alt="qrCode">
                          </div>
                      </div>
                    </section>
                    <footer>
                      <div class="row row2">
                          <div id="whatssap">
                              <img id="picwa" src="assets/images/whatsapp_48px.png" alt="whatssap">
                              <a>+628562294222</a>
                          </div>
                          <div id="gmail">
                              <img id="picgmail" src="assets/images/gmail_48px.png" alt="gmail">
                              <a>info@someah.id</a> 
                          </div>
                      </div>
                    </footer>
                  </div>
                </div>
              </div>
              <div id="buka" class="fixed-top"></div>
              <div class="download-button">
                  <!-- <br> <br>  -->
                  <center class="fixed-bottom mb-5">
                    <a id="download" class="btn btn-primary">
                      <button class="btn" ><li class="fa fa-download"></li> DOWNLOAD </button> 
                    </a>
                  </center> <br> <br>
              </div>
            </div>
</body>
</html>